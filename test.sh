#!/bin/sh

function log(){
    local MY_NAME=${MY_NAME:-$0}
    logger -t ${MY_NAME} -- $@
    echo "$(date) ${MY_NAME}: $@"
}


function trap_exit(){
    log "Trapped error on line $(caller)"
    log "result: failure"
    exit 1
}

function check_http_return_code(){
    export MY_NAME=${FUNCNAME[0]}
    trap "trap_exit ${MY_NAME}" ERR

    [ -n $1 ] # url
    [ -n $2 ] # expected return code

    STATUS_CODE=$(curl -o /dev/null --silent --head --write-out '%{http_code}\n' $1)
    if [ "$STATUS_CODE" == "$2" ];
    then
        log "success: $1 returned $STATUS_CODE"
        return 0
    else
        log "failed: $1 returned $STATUS_CODE (expected $2)"
        return 1
    fi
}

function run_in_background(){
    trap trap_exit ERR
    [ -n "$MY_DIR" ]

    export MY_NAME=${FUNCNAME[0]}
    local LOGFILE=${LOGFILE:-"${MY_DIR}=$(basename $0).log"}

    PASSED=("$@")

    declare -A COMMANDS
    for COMMAND in ${PASSED[@]};
    do
        ${COMMAND} &>>${LOGFILE} &
        COMMANDS[$!]="${COMMAND}"
    done

    SUCCESS=1
    echo -n "Waiting for ${#COMMANDS[@]} to finish... "
    for i in "${!COMMANDS[@]}";
    do
        if wait $i;
        then
            log "success: ${FUNCNAME[0]}: ${COMMANDS[$i]}"
        else
            SUCCESS=0
            log "failure: ${FUNCNAME[0]}: ${COMMANDS[$i]}"
        fi
    done
    if [ "${SUCCESS}" == "1" ];
    then
        return 0
    else
        return 1
    fi
}

check_http_return_code http://localhost:8080 302
check_http_return_code http://localhost:8080/ 302
check_http_return_code http://localhost:8080/index.html 302
check_http_return_code http://localhost:8080/.auth/ 302
check_http_return_code http://localhost:8080/.auth/auth.php 302
check_http_return_code http://localhost:8080/.auth/auth.php?file=index.html 302
check_http_return_code http://localhost:8080/.auth/auth.php?file=non-existant 302
check_http_return_code http://localhost:8080/pages/404.html 200

echo Success.

