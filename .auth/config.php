<?php

    $page_404 = "../pages/404.html";
    $page_401 = "../pages/401.html";

    # https://console.developers.google.com/apis/credentials/
    $google_id = 'your-id.apps.googleusercontent.com';
    $google_secret = 'not-really-a-secret';

    $default_index = 'index.html';

    $allowed_users = [
        "michal.novacek@gmail.com",
        "mnovacek@redhat.com"
    ];

?>
