<?php

    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);

    require_once 'hybrid-auth/src/autoload.php';

    require_once 'mime-types.php';
    require_once 'config.php';

    $path = $_REQUEST['file'];
    if (!isset($path)){ $path=$default_index; }
    $filepath = "../$path";

    if (is_dir($filepath)) {
        $filepath = "$filepath$default_index";
    }

    # If the file is not found, print an error message.
    # You may want to redirect to a standard 404 page.
    if (!is_file($filepath)) {
        header("HTTP/1.1 404 Not Found");
        include $page_404;
        echo "$filepath not found";
        die;
    }

    $parts = explode('.', $filepath);
    $ext = strtolower(array_pop($parts));
    $mime = isset($mime_types[$ext]) ? $mime_types[$ext] : 'text/plain';

    # By default, everything expires 1 week later
    $expires = 60*60*24*7;

    # ... except for specific MIME types
    # See https://github.com/h5bp/html5-boilerplate/blob/master/.htaccess
    if (strstr($mime, 'text/') ||
        strstr($mime, 'application/xml') ||
        strstr($mime, 'application/json')) {
            $expires = 0;
    }

    $config = [
        'callback' => Hybridauth\HttpClient\Util::getCurrentUrl(),
        'keys'     => [
            'id' => $google_id,
            'secret' => $google_secret
        ],
    ];

    $adapter = new Hybridauth\Provider\Google($config);

    try{
        $adapter->authenticate();

        $userProfile = $adapter->getUserProfile();
        $tokens = $adapter->getAccessToken();
    }
    catch (Exception $e){
        header("HTTP/1.1 401 Not Authorized");
        include $page_401;
        die;
    }

    if (! in_array($userProfile->email, $allowed_users)){
        header("HTTP/1.1 401 Unauthorized");
        include $page_401;
        echo "$userProfile->email is not one of allowed users " . print_r($allowed_users);
        die;
    }

    if(!isset($_GET['file'])){
        header('Content-Type: text/html');
        echo "Do not access .auth/auth.php directly.";
        die;
    }

    # Hybrid_Auth uses session_id() which disables caching of content.
    # Let's allow at least private caching
    # http://www.php.net/manual/en/function.session-cache-limiter.php
    session_cache_expire($expires/60);
    session_cache_limiter('private_no_expire');

    # PHP files are served as-is
    if ('php' == $ext) {
        include $filepath;
    } else {
    # Serve the file with the right Content-type and Expires
        header("Content-type: $mime");
        $now = time();
        $last_modified_time = filemtime($filepath);
        header("Last-Modified: ".gmdate("D, d M Y H:i:s", $last_modified_time)." GMT");
        header('Expires: '.gmdate('D, d M Y H:i:s \G\M\T', $now + $expires));
        readfile($filepath);
    }

