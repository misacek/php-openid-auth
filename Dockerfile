FROM php:7.0-apache

COPY . /var/www/html/
RUN \
    echo "<?php phpinfo(); ?>" > /var/www/html/phpinfo.php && \
    echo "wheee!" > /var/www/html/index.html && \
    a2enmod rewrite
